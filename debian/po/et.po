# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Estonian translation of Debian-installer
#
# This translation is released under the same licence as the debian-installer.
#
# Siim Põder <siim@p6drad-teel.net>, 2007.
#
# Thanks to following Ubuntu Translators for review and fixes:
#     Laur Mõtus
#     Heiki Nooremäe
#     tabbernuk
#
#
# Translations from iso-codes:
#   Tobias Quathamer <toddy@debian.org>, 2007.
#     Translations taken from ICU SVN on 2007-09-09
#   Alastair McKinstry <mckinstry@computer.org>, 2001,2002.
#   Free Software Foundation, Inc., 2000, 2004, 2006
#   Hasso Tepper <hasso@estpak.ee>, 2006.
#   Margus Väli <mvali@hot.ee>, 2000.
#   Siim Põder <windo@p6drad-teel.net>, 2006.
#   Tõivo Leedjärv <leedjarv@interest.ee>, 2000, 2001, 2008.
#   Mattias Põldaru <mahfiaz@gmail.com>, 2009-2012, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:32+0000\n"
"PO-Revision-Date: 2012-01-25 02:09+0200\n"
"Last-Translator: Mattias Põldaru <mahfiaz@gmail.com>\n"
"Language-Team: Estonian <>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

#. Type: select
#. Description
#: ../elilo-installer.templates:1001
msgid "Partition for boot loader installation:"
msgstr "Partitsioon alglaaduri paigaldamiseks:"

#. Type: select
#. Description
#: ../elilo-installer.templates:1001
msgid ""
"Partitions currently available in your system are listed. Please choose the "
"one you want elilo to use to boot your new system."
msgstr ""
"Nimekirjas on loetletud praegu kättesaadavad süsteemi partitsioonid. Vali, "
"millist neist võib kasutada elilo alglaadimiseks."

#. Type: error
#. Description
#: ../elilo-installer.templates:2001
msgid "No boot partitions detected"
msgstr "Ei tuvastatud ühtki alglaaduripartitsiooni"

#. Type: error
#. Description
#: ../elilo-installer.templates:2001
msgid ""
"There were no suitable partitions found for elilo to use.  Elilo needs a "
"partition with a FAT file system, and the boot flag set."
msgstr ""
"Ei leitud ühtki elilo jaoks sobivat partitsiooni. Elilo vajab "
"alglaadimislipuga FAT failisüsteemi."

#. Type: text
#. Description
#. Main menu item
#: ../elilo-installer.templates:3001
msgid "Install the elilo boot loader on a hard disk"
msgstr "Elilo alglaaduri kõvakettale paigaldamine"

#. Type: text
#. Description
#: ../elilo-installer.templates:4001
msgid "Installing the ELILO package"
msgstr "Paketi ELILO Paigaldamine"

#. Type: text
#. Description
#: ../elilo-installer.templates:5001
msgid "Running ELILO for ${bootdev}"
msgstr "ELILO käivitamine ${bootdev} jaoks"

#. Type: boolean
#. Description
#: ../elilo-installer.templates:6001
msgid "ELILO installation failed.  Continue anyway?"
msgstr "ELILO paigaldamine nurjus. Kas jätkata sellele vaatamata?"

#. Type: boolean
#. Description
#: ../elilo-installer.templates:6001
msgid ""
"The elilo package failed to install into /target/.  Installing ELILO as a "
"boot loader is a required step.  The install problem might however be "
"unrelated to ELILO, so continuing the installation may be possible."
msgstr ""
"Paketi elilo /target/ peale paigaldamine nurjus. Alglaaduri ELILO "
"paigaldamine on küll vajalik samm, kuid ilmnenud probleem ei pruugi üldse "
"ELILO'ga seotud olla, mistõttu võib osutuda võimalikuks jätkata."

#. Type: error
#. Description
#: ../elilo-installer.templates:7001
msgid "ELILO installation failed"
msgstr "ELILO paigaldamine nurjus"

#. Type: error
#. Description
#: ../elilo-installer.templates:7001
msgid "Running \"/usr/sbin/elilo\" failed with error code \"${ERRCODE}\"."
msgstr "\"/usr/sbin/elilo\" käivitamine andis veateate \"${ERRCODE}\"."
